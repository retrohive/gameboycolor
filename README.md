# gameboycolor

based on https://archive.org/details/no-intro_romsets (20210223-010902)

ROM not added :
```
❯ tree -hs --dirsfirst
.
├── [2.0K]  [BIOS] Nintendo Game Boy Color Boot ROM (Japan) (En).zip
├── [2.0K]  [BIOS] Nintendo Game Boy Color Boot ROM (World) (Rev 1).zip
├── [247K]  Carnivale (USA) (Proto).zip
├── [ 12K]  Crazy Climber (USA) (Proto 1) (2000-09-22).zip
├── [ 26K]  Crazy Climber (USA) (Proto 2) (2000-11-21).zip
├── [1.2M]  Gimmick Land (Japan) (Proto).zip
├── [974K]  Hajimari no Mori (Japan) (Proto).zip
├── [440K]  Jibaku-kun - Rei no Itsuki no Kajitsu (Japan) (Proto).zip
├── [486K]  Max Steel - Covert Missions (USA) (En,Fr,De,Es) (Proto).zip
├── [158K]  Moorhuhn 3 - ...Es Gibt Huhn! (Europe) (En,Fr,De,Es,It).zip
├── [466K]  Pokemon Picross (Japan) (Proto).zip
├── [ 19K]  Star Trek (Europe) (Proto).zip
├── [288K]  Sutte Hakkun GB (Japan) (Proto).zip
└── [937K]  Trade & Battle Card Hero (Japan) (Rev 1) (3DS Virtual Console) (SGB Enhanced) (GB Compatible).zip

0 directories, 14 files
```